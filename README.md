## install
- install rclone
- install WinFsp (on Windows only)
- install git


## mount cloud
`rclone mount REMOTE:[path] LOCAL_PATH --vfs-cache-mode writes`  
`# or use a drive letter with --network-mode on Windows only`  
`rclone mount REMOTE:[path] X: --network-mode --vfs-cache-mode writes`

## copy to cloud
`rclone --bwlimit 400k sync --update --retries 1 --links "localDir" "REMOTE:RemoteDir"`  
limit the bandwidth if needed with `--bwlimit BwTimetable`  
skip files that are newer on the destination with `--update` (useful by copy to the cloud)  
translate symlinks to/from regular files with `--links`

## init git
`cd LOCAL_PATH` or `cd X:`
```
git init
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
git config --global gc.auto 0
```

## usage
### use mount
start "mount_rc.sh"  
Ctrl-C for break

### use copy
put dir path(es) to "dirs_to_copy.txt"  
start "copy_rc.sh" or "copy_rc_to_log.sh"

The script will check the status of files on the cloud
and warn if there are changes not from this script.
Then it will copy the dirs from the "dirs_to_copy.txt" file,
and save changes to version history.
