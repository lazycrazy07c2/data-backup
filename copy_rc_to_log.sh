#!/bin/sh

LOG_DIR="log"
LOG_FILE="log.txt"


WORK_DIR="$(pwd)"
mkdir -p "$LOG_DIR"
LOG_FILE="$WORK_DIR/$LOG_DIR/$LOG_FILE"

echo "" >> "$LOG_FILE"
echo "$(date "+%F %T")" >> "$LOG_FILE"

bash copy_rc.sh 2>&1 | tee -a "$LOG_FILE"

echo "" >> "$LOG_FILE"
