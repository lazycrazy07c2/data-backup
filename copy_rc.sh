#!/bin/bash

RC_CMD="rclone" # path to CMD
RC_REMOTE="remote:" # name-of-remote:[path]
LOCAL_MOUNT_POINT="/mnt" # local-path (use /X for X: on Windows only)
LOCAL_CLOUD_COPY="/cloud" # local-path for the cloud copy (to make the history local, needs 3x local space)
DIRS_TO_COPY_FILE="dirs_to_copy.txt"
LOG_DIR="log"
LOG_FILE="log.txt"


SECONDS=0
WORK_DIR="$(pwd)"
mkdir -p "$LOG_DIR"
LOG_FILE="$WORK_DIR/$LOG_DIR/$LOG_FILE"

git_status() {
    if [ ! -d ".git" ]; then
        echo "no '.git' dir exists"
        read -p "create git history (y/N) " INPUT
        echo "$INPUT" >> "$LOG_FILE"
        if [ "y" != "$INPUT" ]; then
            return
        fi
        git init
    fi
    # check the status
    echo "checking status"
    GIT_ST="$(git status -s)"
    if [ "$?" -ne 0 ]; then
        echo "ERROR on checking status"
        exit 1
    fi
    if [ ! -z "$GIT_ST" ]; then
        echo "WARN: the files below will be overwritten"
        echo "$GIT_ST"
        read -p "continue (y/N) " INPUT
        echo "$INPUT" >> "$LOG_FILE"
        if [ "y" != "$INPUT" ]; then
            exit 0
        fi
    fi
}

# normalize lines (remove \r) and empty lines
sed -i 's/\r//' "$DIRS_TO_COPY_FILE"
sed -i '/^$/d' "$DIRS_TO_COPY_FILE"

# if to make a remote history takes too long
if [ ! -z "$LOCAL_CLOUD_COPY" ]; then
    # copy the cloud to local cloud copy
    TIMEFORMAT='copy the cloud to local copy took %0lR.'
    time {
        # copy the '.git' dir to local copy
        REMOTE="$RC_REMOTE.git"
        LOCAL_COPY="$LOCAL_CLOUD_COPY/.git"
        echo "copy '$REMOTE' to the dir '$LOCAL_COPY'"
        "$RC_CMD" sync --retries 1 --links "$REMOTE" "$LOCAL_COPY"
        # copy the cloud to local copy
        while read -r LINE; do
            LINE="${LINE/:}" # delete first ':' (useful for Windows paths only)
            REMOTE="$RC_REMOTE${LINE//\\/\/}" # replace all \ with / (useful for Windows paths only)
            LOCAL_COPY="$LOCAL_CLOUD_COPY/$LINE"
            echo "copy '$REMOTE' to the dir '$LOCAL_COPY'"
            "$RC_CMD" sync --retries 1 --links "$REMOTE" "$LOCAL_COPY"
        done < "$DIRS_TO_COPY_FILE"
    }

    cd "$LOCAL_CLOUD_COPY"
    git_status
    cd "$WORK_DIR"

    # copy changes to local cloud copy
    TIMEFORMAT='local copy took %0lR.'
    time {
        while read -r LINE; do
            LOCAL_COPY="$LOCAL_CLOUD_COPY/${LINE/:}" # delete first ':' (useful for Windows paths only)
            echo "copy the dir '$LINE' to the dir '$LOCAL_COPY'"
            "$RC_CMD" sync --update --retries 1 --links "$LINE" "$LOCAL_COPY"
        done < "$DIRS_TO_COPY_FILE"
    }

    # save to version history
    echo "save all changes to version history"
    TIMEFORMAT='save to history took %0lR.'
    time {
        cd "$LOCAL_CLOUD_COPY"

        git add --all
        echo "added to version history"

        git commit -a -m "automatic backup"
        echo "saved to version history"

        cd "$WORK_DIR"
    }

    # copy the local cloud copy to the cloud
    TIMEFORMAT='copy to the cloud took %0lR.'
    time {
        # copy the '.git' dir to the cloud
        LOCAL_COPY="$LOCAL_CLOUD_COPY/.git"
        REMOTE="$RC_REMOTE.git"
        echo "copy the dir '$LOCAL_COPY' to '$REMOTE'"
        "$RC_CMD" sync --update --retries 1 --links "$LOCAL_COPY" "$REMOTE"
        # copy changes
        while read -r LINE; do
            LINE="${LINE/:}" # delete first ':' (useful for Windows paths only)
            LOCAL_COPY="$LOCAL_CLOUD_COPY/$LINE"
            REMOTE="$RC_REMOTE${LINE//\\/\/}" # replace all \ with / (useful for Windows paths only)
            echo "copy the dir '$LOCAL_COPY' to '$REMOTE'"
            "$RC_CMD" sync --update --retries 1 --links "$LOCAL_COPY" "$REMOTE"
        done < "$DIRS_TO_COPY_FILE"
    }
else
    cd "$LOCAL_MOUNT_POINT" 2>/dev/null
    if [ "$?" -ne 0 ]; then
        sh mount_rc.sh &> /dev/null &

        while [ "$(pwd)" != "$LOCAL_MOUNT_POINT" ]; do
            echo "waiting for mount..." >> "$LOG_FILE"
            sleep 2
            cd "$LOCAL_MOUNT_POINT" 2>/dev/null
        done
    fi

    # check the lock file exists
    if [ -f ".git/index.lock" ]; then
        echo "WARN: .git/index.lock exists. Another process seems to be running. Please make sure all processes are terminated. If a process has crashed earlier: remove the file."
        read -p "remove file (y/N) " INPUT
        echo "$INPUT" >> "$LOG_FILE"
        if [ "y" != "$INPUT" ]; then
            exit 0
        fi
        rm ".git/index.lock"
        while [ -f ".git/index.lock" ]; do
            echo ".git/index.lock exists. waiting..." >> "$LOG_FILE"
            sleep 2
        done
    fi

    git_status

    cd "$WORK_DIR" || exit 1
    # copy to cloud
    TIMEFORMAT='copy took %0lR.'
    time {
        while read -r LINE; do
            DIR="${LINE//\\/\/}" # replace all \ with / (useful for Windows paths only)
            REMOTE="$RC_REMOTE${DIR/:}" # delete first ':' (useful for Windows paths only)
            echo "copy the dir '$LINE' to '$REMOTE'"
            "$RC_CMD" sync --update --retries 1 --links "$LINE" "$REMOTE"
        done < "$DIRS_TO_COPY_FILE"
    }

    cd "$LOCAL_MOUNT_POINT" || exit 1
    # save to version history
    echo "save all changes to version history"
    TIMEFORMAT='save to history took %0lR.'
    time {
        while [ -f ".git/index.lock" ]; do
            echo ".git/index.lock exists. waiting..." >> "$LOG_FILE"
            sleep 2
        done

        git add --all
        echo "added to version history"

        while [ -f ".git/index.lock" ]; do
            echo ".git/index.lock exists. waiting..." >> "$LOG_FILE"
            sleep 2
        done

        git commit -a -m "automatic backup"
        echo "saved to version history"
    }
fi


echo "done"
echo "it took $((${SECONDS}/60))m$((${SECONDS}%60))s"
read -p "continue ..." INPUT
echo "$INPUT" >> "$LOG_FILE"
