#!/bin/sh

RC_CMD="rclone" # path to CMD
RC_REMOTE="remote:" # name-of-remote:[path]
LOCAL_MOUNT_POINT="/mnt" # local-path (can be X: on Windows only)

# mount $RC_REMOTE to $LOCAL_MOUNT_POINT
echo "mounting $RC_REMOTE to $LOCAL_MOUNT_POINT"
"$RC_CMD" mount "$RC_REMOTE" "$LOCAL_MOUNT_POINT" --network-mode --vfs-cache-mode full # --network-mode (on Windows only)

read -p "continue ..." INPUT
