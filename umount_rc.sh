#!/bin/sh

# umount
MOUNT_ID=$(ps -s | grep "rclone" | grep -o -m1 " [0-9]\+ " | cut -d' ' -f2)

if [ ! -z "$MOUNT_ID" ]; then
	echo "kill $MOUNT_ID"
	kill "$MOUNT_ID"
else
	echo "nothing to umount"
fi

echo "done"
read -p "continue ..." INPUT
